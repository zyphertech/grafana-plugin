# Load Dummy Data
- `create database dummy_data`
- `mysql -u root -p dummy_data < schema.sql`
- `mysql -u root -p dummy_data < query.sql`
- Add datasource called mysql in grafana
