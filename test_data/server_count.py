import random

apps = range(1, 6)
availablity_zones = range(1, 17)

for i in range(10000):
    sql_query_template = "INSERT INTO SERVERS_COUNT (id, availibty_zone_id, app_id, count) VALUES ({}, {}, {}, {});"
    print(sql_query_template.format(
        i,
        random.choice(availablity_zones),
        random.choice(apps),
        random.randint(1, 100)
    ))
