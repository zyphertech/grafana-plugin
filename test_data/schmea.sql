CREATE TABLE APP (
    id int primary key,
    app_name varchar(100)
);

CREATE TABLE REGION (
    id int primary key,
    region_name varchar(100)
);

CREATE TABLE AVAILABLITY_ZONES (
    id int primary key,
    availibty_zone_name varchar(100),
    region_id int references REGION(id)
);

CREATE TABLE SERVERS_COUNT (
    id int primary key,
    availibty_zone_id int references AVAILABLITY_ZONES(id),
    app_id int references APP(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    count int
);

INSERT INTO APP VALUES (1, 'mismo');
INSERT INTO APP VALUES (2, 'kraken');
INSERT INTO APP VALUES (3, 'lilith');
INSERT INTO APP VALUES (4, 'izin');
INSERT INTO APP VALUES (5, 'kombi');

INSERT INTO REGION VALUES (1, 'USA');
INSERT INTO REGION VALUES (2, 'India');
INSERT INTO REGION VALUES (3, 'Ireland');
INSERT INTO REGION VALUES (4, 'Abu Dhabi');

insert into AVAILABLITY_ZONES values (1, 'us-west-1', 1);
insert into AVAILABLITY_ZONES values (2, 'us-west-2', 1);
insert into AVAILABLITY_ZONES values (3, 'us-east-2', 1);
insert into AVAILABLITY_ZONES values (4, 'us-east-1', 1);

insert into AVAILABLITY_ZONES values (5, 'in-west-1', 2);
insert into AVAILABLITY_ZONES values (6, 'in-west-2', 2);
insert into AVAILABLITY_ZONES values (7, 'in-east-2', 2);
insert into AVAILABLITY_ZONES values (8, 'in-east-1', 2);

insert into AVAILABLITY_ZONES values (9, 'eu-west-1', 3);
insert into AVAILABLITY_ZONES values (10, 'eu-west-2', 3);
insert into AVAILABLITY_ZONES values (11, 'eu-east-2', 3);
insert into AVAILABLITY_ZONES values (12, 'eu-east-1', 3);

insert into AVAILABLITY_ZONES values (13, 'aud-west-1', 4);
insert into AVAILABLITY_ZONES values (14, 'aud-west-1', 4);
insert into AVAILABLITY_ZONES values (15, 'aud-west-1', 4);
insert into AVAILABLITY_ZONES values (16, 'aud-west-1', 4);
